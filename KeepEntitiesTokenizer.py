import spacy
from functools import reduce
import pandas as pd
import numpy as np
import re
import csv
import pickle

from pandas.core import series
from pprint import pprint
from gensim.models import Word2Vec


# A wrapper class of the NLP tokenizer class
class KeepEntitiesTokenizer:
    symbolRegex = re.compile(r"[^a-zA-Z]+")

    @staticmethod
    def getEntityType(inputSpanOrToken):
        if isinstance(inputSpanOrToken, spacy.tokens.token.Token):
            return inputSpanOrToken.ent_type_
        elif isinstance(inputSpanOrToken, spacy.tokens.span.Span):
            return inputSpanOrToken.label_
        else:
            raise IOError("Expected input class is spacy span or token. ", "Received input type: ",
                          type(inputSpanOrToken))

    @staticmethod
    def convertToStr(input):
        if isinstance(input, (spacy.tokens.token.Token, spacy.tokens.span.Span)):
            return input.text
        if isinstance(input, list):
            return list(map(lambda x: x.text, input))

    def __init__(self, chunkTypesKept):
        self.nlp = spacy.load('en_core_web_lg')
        self.spacy_stopwords = spacy.lang.en.stop_words.STOP_WORDS
        self.chunkTypesKept = chunkTypesKept

    def isStopwordOrSymbol(self, inputStr):
        if inputStr.lower() in self.spacy_stopwords:
            return True
        if pd.notnull(self.__class__.symbolRegex.match(inputStr)):
            return True
        return False

    def tokenize(self, inputSent):
        if pd.isnull(inputSent):
            return []
        elif not isinstance(inputSent, str):
            raise IOError("Expected input class str, Received input type", str(type(inputSent)))
        elif len(inputSent) == 0:
            return []

        curDoc = self.nlp(inputSent)
        entities = list(filter(lambda x: self.__class__.getEntityType(x) in self.chunkTypesKept, curDoc.ents))
        # entityIndices = reduce(list.__add__, list(map(lambda x: [x.start, x.end], entities)))
        entityBeginIndices = list(map(lambda x: x.start, entities))
        resultList = []

        i = 0
        while i < len(curDoc):
            if i not in entityBeginIndices:
                resultList.append(curDoc[i])
                i = i + 1
            else:
                curEntity = entities[0]
                # Filter the beginning & ending stop word, when len > 2
                if curEntity.__len__() == 1:
                    # Do nothing
                    resultList.append(entities[0])
                elif curEntity.__len__() == 2:
                    # If any of the two tokens is a stop word, break this entity into two
                    if self.isStopwordOrSymbol(curEntity[0].text) or self.isStopwordOrSymbol(curEntity[-1].text):
                        resultList.append(curEntity[0])
                        resultList.append(curEntity[1])
                    else:
                        resultList.append(entities[0])
                else:  # If len >= 3
                    if self.isStopwordOrSymbol(curEntity[0].text):
                        resultList.append(curEntity[0])
                        curEntity = curEntity[1:]
                    if self.isStopwordOrSymbol(curEntity[-1].text):
                        resultList.append(curEntity[:-1])
                        resultList.append(curEntity[-1])
                    else:
                        resultList.append(curEntity)
                i = entities[0].end
                del (entities[0])
        return resultList

    def tokenizeAndFilterStopwords(self, inputSent):
        tokenizedList = self.tokenize(inputSent)
        return list(filter(lambda x: not self.isStopwordOrSymbol(x.text), tokenizedList))

    def tokenizeAndFilterSymbolsOnly(self, inputSent):
        tokenizedList = self.tokenize(inputSent)
        return list(filter(lambda x: pd.isnull(self.__class__.symbolRegex.match(x.text)), tokenizedList))
