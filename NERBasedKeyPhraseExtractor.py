import spacy
import pandas as pd
import pickle
import numpy as np


class NERBasedKeyPhraseExtractor:
    # Constructor
    @staticmethod
    def CombineEntitiesAndNounChunks(entities, noun_chunks):
        result_1 = []  # The highest priority result list
        for cur_noun_chunk in noun_chunks:
            for cur_entity in entities:
                if cur_entity.text in cur_noun_chunk.text:
                    result_1.append(cur_noun_chunk)
                    break;
        # The secondary list, filter out those that exactly match with what is in result_1
        result_2 = list(filter(lambda x: x.text not in list(map(lambda y: y.text, noun_chunks)), entities))
        # The third list, the remaining phrases.
        result_3 = [x for x in noun_chunks if x not in result_1]
        return [result_1, result_2, result_3]  # In descending order of importance

    @staticmethod
    def convertToString(keyPhrases):
        strList = []
        for curMinuteLists in keyPhrases:
            curLists = []
            for curList in curMinuteLists:
                if len(curList) == 0:
                    curLists.append([])
                else:
                    tempList = list(map(lambda x: str(x), curList))
                    curLists.append(tempList)
            strList.append(curLists)
        return strList

    def __init__(self):
        self.nlp = spacy.load('en_core_web_lg')
        self.spacy_stopwords = spacy.lang.en.stop_words.STOP_WORDS

    def extract(self, inputSent):
        processed = self.nlp(inputSent)
        entities = processed.ents  # NE extracted by NLP
        noun_chunks = [x for x in processed.noun_chunks]
        processed_nouns = self.__class__.CombineEntitiesAndNounChunks(entities, noun_chunks)
        self.filterInitialStopWords(processed_nouns)
        return processed_nouns
        return self.__class__.convertToString(processed_nouns)

    def filterInitialStopWords(self, processed_nouns):
        phraseList0 = processed_nouns[0]
        phraseList1 = processed_nouns[1]
        phraseList2 = processed_nouns[2]
        filteredList0 = list(map(lambda x: x[1:] if x[0].text.lower() in self.spacy_stopwords else x, phraseList0)) \
            if len(phraseList0) > 0 else phraseList0
        filteredList1 = list(map(lambda x: x[1:] if x[0].text.lower() in self.spacy_stopwords else x, phraseList1)) \
            if len(phraseList1) > 0 else phraseList1
        filteredList2 = list(map(lambda x: x[1:] if x[0].text.lower() in self.spacy_stopwords else x, phraseList2)) \
            if len(phraseList2) > 0 else phraseList2
        processed_nouns[0] = filteredList0
        processed_nouns[1] = filteredList1
        processed_nouns[2] = filteredList2
