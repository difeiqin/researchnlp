from NERBasedKeyPhraseExtractor import *
from KeepEntitiesTokenizer import *
from pprint import pprint
import pickle

# chunkTypesKept = ["ORG", "GPE", "PERSON"]
chunkTypesKept = ["ORG", "GPE"]
tokenizer = KeepEntitiesTokenizer(chunkTypesKept)
# Some toy examples.
tokenizer.tokenize("This is the White House Washington, United States. Where Abraham Lincoln once lived.")
tokenizer.tokenizeAndFilterStopwords("This is the White House Washington, New Jersey. Where Abraham Lincoln once lived.")
tokenizer.tokenizeAndFilterSymbolsOnly("This is the White House Washington, New Jersey. Where Abraham Lincoln once lived.")


## Now, Train W2V model using this new KeepEntitiesTokenizer
df1 = pd.read_csv("../data/all-the-news/articles1.csv")
df2 = pd.read_csv("../data/all-the-news/articles2.csv")
df3 = pd.read_csv("../data/all-the-news/articles3.csv")
df = pd.concat([df1, df2, df3], axis=0).reset_index(drop=True)
index_arr = np.array(pd.read_csv("../data/index_to_use.csv", names=["index"]).loc[:, "index"]).astype(np.int).tolist()
contentAndTitleSeries = (df['title'] + " " + df['content'])[index_arr]
contentAndTitleSeries.reset_index(drop=True, inplace=True)

tokenizedTitleAndContent = []
for i in range(len(contentAndTitleSeries)):
    tokenizedTitleAndContent.append(tokenizer.tokenizeAndFilterSymbolsOnly(contentAndTitleSeries[i]))
    print("Finish... " + str(i) + "-th tokenization")

# Convert all the results to string format
tokenizedTitleAndContentStr = []
for i in range(len(contentAndTitleSeries)):
    tokenizedTitleAndContentStr.append(tokenizer.convertToStr(tokenizedTitleAndContent[i]))
    print("Finish... " + str(i) + "-th tokenization")

# Save the result in strings
with open("../result/tokenizedTitleAndContentStr", "wb") as f:
    pickle.dump(tokenizedTitleAndContentStr, f)