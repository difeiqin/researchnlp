import pickle
import gensim
from gensim.models import Word2Vec

# with open("../result/tokenizedTitleAndContentStr", "rb") as f:
#     w2vTrainingData = pickle.load(f)

with open("./data/tokenizedTitleAndContentStr_datetime.datetime(2018, 10, 1, 8, 53, 37, 469576)", "rb") as f:
    w2vTrainingData1 = pickle.load(f)
with open("./data/tokenizedTitleAndContentStr_datetime.datetime(2018, 10, 1, 8, 53, 55, 26104)", "rb") as f:
    w2vTrainingData2 = pickle.load(f)
with open("./data/tokenizedTitleAndContentStr_datetime.datetime(2018, 10, 1, 8, 54, 13, 373682)", "rb") as f:
    w2vTrainingData3 = pickle.load(f)

w2v_complete_model_news_with_entities = Word2Vec(w2vTrainingData1 + w2vTrainingData2 + w2vTrainingData3, size=100, window=5,
                                        min_count=5, workers=4)
w2v_complete_model_news_with_entities.save("./result/myModel-complete-corpus-KeepEntitiesTokenizer")

# SO gensim is already using Cython (i.e. the fast version = 1)
gensim.models.word2vec.FAST_VERSION

# What slows the current training process down is not the training itself,
# but rather the spaCy tokenization step. (try multi-threading)
# Can Google Cloud Platform speeds it up?
# Let's fix the similarity & tokenization corner cases first, and then test the tokenization on GCP.
