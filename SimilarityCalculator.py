from TFIDFKeywordExtractor import *
import numpy as np
import warnings


class SimilarityCalculator:
    def __init__(self, trainningTokenLists, wv, topK, stopwords):
        self.tfidf = TFIDFKeywordExtractor(trainningTokenLists, stopwords)
        self.allKeywords = np.array(self.tfidf.getKeywords(trainningTokenLists, topK))
        self.wv = wv
        self.filteredTrainingKeywords = []
        for curKeywordList in self.allKeywords:
            self.filteredTrainingKeywords.append(list(filter(lambda x: self.wv.__contains__(x), curKeywordList)))

    def getMostSimilarbyW2VforSingleList(self, tokenList, topK):
        # Filter the self.allKeywords first, based on wv
        # tokenList is a list of tokens. Need to check wether each token is in wv or not.
        filteredTokenList = list(filter(lambda x: x in self.wv, tokenList))
        scoreList = np.array(self.calculateSimForAllTrainList(filteredTokenList))
        # descIndex = np.flip(scoreList.argsort())[:topK] # Need to consider nan case. thus cannot simply flip, neet to sort reverse
        descIndex = np.argsort(scoreList * -1)[:topK]
        return descIndex, self.allKeywords[descIndex], scoreList[descIndex]

    def calculateSimForAllTrainList(self, tokenList):
        scoreList = []
        for i, curKeywordList in enumerate(self.filteredTrainingKeywords):
            scoreList.append(self.calculateSimForOneTrainList(tokenList, curKeywordList))
            # print(str(i) + "-th score is...", scoreList[-1])
        return scoreList

    def calculateSimForOneTrainList(self, tokenList, curKeywordList):
        # scores = np.empty(shape=(0, len(tokenList)), dtype=np.float64)
        scores = []
        for i, curToken in enumerate(
                tokenList):  # For each token, calculate the mean similarity along the whole keywordlist
            scores.append(np.nanmean(
                list(map(lambda x: self.wv.similarity(curToken, x) if x in self.wv else np.nan, curKeywordList))))
        return np.nanmean(scores)

    def printkeywordsAndScores(self, matchedKeywordList, scoreList):
        for i in range(len(scoreList)):
            print(matchedKeywordList[i])
            print("Mean similarity score for this doc: ", scoreList[i])
            print("\n")
