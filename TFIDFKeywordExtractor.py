# For the training data (i.e. tokenizedTitleAndContentStr)
# Calculate the tf-idf score for each word and save the result.
from sklearn.feature_extraction.text import TfidfVectorizer
import numpy as np
import spacy

class TFIDFKeywordExtractor:
    def __init__(self, trainingTokenList, stopwords):
        self.nativeTfidf = TfidfVectorizer(analyzer="word", tokenizer=lambda x: x, preprocessor=lambda x: x,
                                           token_pattern=None)
        self.stopwords = stopwords
        filteredTrainingTokenList = list(map(lambda curL: self.filterOutStopwords(curL), trainingTokenList))
        self.nativeTfidf.fit(filteredTrainingTokenList)
        self.features = np.array(self.nativeTfidf.get_feature_names())

    def getKeywordsandScores(self, tokenList, topK):
        filteredTokenList = list(map(lambda curL:self.filterOutStopwords(curL), tokenList))
        scoreMatrix = self.nativeTfidf.transform(filteredTokenList)  # sparse matrix
        sortedIndices = []
        for index ,r in enumerate(scoreMatrix):
            sortedIndices.append(sorted(r.indices, key=lambda i: r[0, i], reverse=True))
            print("Finish sorting " + index + "-th keywords from training data")

        resultList = []
        for i, r in enumerate(scoreMatrix):
            K = topK if r.count_nonzero() > topK else r.count_nonzero()
            indices = sortedIndices[i][:K]
            curScores = r[0, indices].toarray()[0]
            curFeatures = self.features[indices]
            resultList.append(list(lambda x, y: (x, y), curFeatures, curScores))
            print("Appended " + index + "-th keywords from training data")
        return resultList

    def getKeywords(self, tokenList, topK):
        filteredTokenList = list(map(lambda curL:self.filterOutStopwords(curL), tokenList))
        scoreMatrix = self.nativeTfidf.transform(filteredTokenList)  # sparse matrix
        sortedIndices = []
        # print("Finish native TFIDF transformation")
        for r in scoreMatrix:
            sortedIndices.append(sorted(r.indices, key=lambda i: r[0, i], reverse=True))
        # print("Finish native sorting index")

        resultList = []
        for i, r in enumerate(scoreMatrix):
            K = topK if r.count_nonzero() > topK else r.count_nonzero()
            indices = sortedIndices[i][:K]
            curFeatures = self.features[indices]
            resultList.append(curFeatures)
            # print("Finish... ", i)
        return resultList

    def filterOutStopwords(self, inputList):
        return list(filter(lambda x: x not in self.stopwords, inputList))
