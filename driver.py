import pickle
import pandas as pd
import numpy as np
from NERBasedKeyPhraseExtractor import *
from gensim.models import Word2Vec
from pprint import pprint
from TFIDFKeywordExtractor import *
from SimilarityCalculator import *
import spacy


## A demostration of how the current similarity calculator works.
with open("./data/tokenizedTitleAndContentStr_datetime.datetime(2018, 10, 1, 8, 53, 37, 469576)", "rb") as f:
    w2vTrainingData1 = pickle.load(f)
with open("./data/tokenizedTitleAndContentStr_datetime.datetime(2018, 10, 1, 8, 53, 55, 26104)", "rb") as f:
    w2vTrainingData2 = pickle.load(f)
with open("./data/tokenizedTitleAndContentStr_datetime.datetime(2018, 10, 1, 8, 54, 13, 373682)", "rb") as f:
    w2vTrainingData3 = pickle.load(f)
with open("./data/spacy_stopword.pkl", "rb") as f:
    spacy_stopwords = pickle.load(f)

w2v_model_news_with_entities = Word2Vec.load("./result/myModel-complete-corpus-KeepEntitiesTokenizer")
wv = w2v_model_news_with_entities.wv

# initialize simCalculator
simCalculator = SimilarityCalculator(w2vTrainingData1 + w2vTrainingData2 + w2vTrainingData3, wv, 10,
                                     spacy_stopwords)

with open("./result/simCalculator", "wb") as f:
    pickle.dump(simCalculator, f)

# Find the most similar 10 ariticles from the w2vTraning data, output the scores & keywords
descIndex1, keywords1, matchedScores1 = simCalculator.getMostSimilarbyW2VforSingleList(["White House"], 10)
simCalculator.printkeywordsAndScores(keywords1, matchedScores1)

descIndex2, keywords2, matchedScores2 = simCalculator.getMostSimilarbyW2VforSingleList(["White House", "Obama"], 10)
simCalculator.printkeywordsAndScores(keywords2, matchedScores2)

descIndex3, keywords3, matchedScores3 = simCalculator.getMostSimilarbyW2VforSingleList(
    ["White House", "Obama", "Trump"], 10)
simCalculator.printkeywordsAndScores(keywords3, matchedScores3)

# # Debug area...  below
#
# print(scored_text)
# ## Check the length of each tokenized document...? (Should have removed stopwords when trainning, can do this process on GCP)
# pprint(list(map(lambda x: len(x), scored_text)))
# # [131, 0, 40, 177, 31, 917, 781, 851, 764, 1410]
# # After fixing: [917, 781, 851, 764, 1410, 1031, 378, 529, 1455, 1111]
#
# ## Check how many of the tokens are in the wv
# pprint(list(map(lambda x: countInWV(x, wv), scored_text)))
# # [121, 0.0, 20, 159, 21, 914, 778, 847, 764, 1398]
# # After fixing: [914, 778, 847, 764, 1398, 1029, 377, 527, 1452, 1104]
#
# ## Check how many of the keywords are in the wv
# pprint(list(map(lambda x: countInWV(x, wv), keywords)))
# # [0, 0.0, 0, 0, 0, 10, 10, 10, 10, 10], no wonder the scores for top 5 is zero... So I guess it has sth to do with soring...
# # the NA/nan value should not be returned as highest scores...
# # After fixing: [10, 10, 10, 10, 10, 10, 9, 10, 10, 10]
#
# ## Check the scores:
# pprint(matchedScores)
#
# for i in range(10):
#     pprint(keywords[i])
#     print("Similarity Score: " + str(matchedScores[i]))
# # KEYWORD: White House
# #
# # array(['Comey', 'committee', 'Russia', 'Sessions', 'Senate', 'Sessions’',
# #        'intelligence', 'Trump', 'White House', 'Feinstein'], dtype='<U339')
# # Similarity Score: 0.5045115625716388
# #
# # array(['media', 'Trump', 'press', 'inauguration', 'Conway', 'Priebus',
# #        'Sunday', 'crowds', 'White House', 'CIA'], dtype='<U339')
# # Similarity Score: 0.5033703241223053
# #
# # array(['Comey', 'White House', 'president', 'McGahn', 'aides', 'Flynn',
# #        'Trump', 'Yates', 'firing', 'deceit'], dtype='<U339')
# # Similarity Score: 0.5013337191968963
# #
# # array(['Nunes', 'Schiff', 'committee', 'House', 'Russia', 'intelligence',
# #        'White House', 'investigation', 'investigators', 'Senate'],
# #       dtype='<U339')
# # Similarity Score: 0.49372036632785327
# #
# # array(['Epshteyn', 'White House', 'Trump', 'Flynn', 'Hill',
# #        'communications', 'Priebus', 'Wing', 'staff', 'Spicer'],
# #       dtype='<U339')
# # Similarity Score: 0.4891688803955205
# #
# # array(['Kushner', 'Flynn', 'Kislyak', 'meetings', 'probe', 'Russian',
# #        'Mueller', 'Trump', 'December', 'White House'], dtype='<U339')
# # Similarity Score: 0.4856455702653766
# #
# # array(['Dubke', 'White House', 'communications', 'Trump', 'Spicer',
# #        'resignation', 'Priebus', 'remain', 'Crossroads Media',
# #        'president'], dtype='<U339')
# # Similarity Score: 0.4848418977265563
# #
# # array(['FBI', 'McCabe', 'White House', 'communications', 'Priebus',
# #        'contacts', 'investigation', 'Comey', 'CNN', 'Russians'],
# #       dtype='<U339')
# # Similarity Score: 0.4845064317152786
# #
# # array(['Comey', 'Flynn', 'Trump', 'investigation', 'memo', 'FBI',
# #        'White House', 'conversation', 'General', 'fired'], dtype='<U339')
# # Similarity Score: 0.48224636730870196
# #
# # array(['Trump', 'Congress', 'Ryan', 'Cabinet', 'House', 'Representative',
# #        'Price', 'Republican', 'Senate', 'congressional'], dtype='<U339')
# # Similarity Score: 0.4782070721609587
#
#
# for curContent in contentAndTitleSeries[descIndex]:
#     pprint(str(curContent))
#     print("\n")
#
#
# def countInWV(listofwords, wv):
#     return np.sum(np.array(list(map(lambda x: x in wv, listofwords))))
#
#
# tempTokenList = ["White House"]
#
# scores = []
# for i, curToken in enumerate(tempTokenList):
#     scores.append(np.nanmean(list(map(lambda x: wv.similarity(curToken, x) if x in wv else np.nan, keywords[3]))))
#
# list(map(lambda x: wv.similarity(curToken, x) if x in wv else np.nan, keywords[3]))
