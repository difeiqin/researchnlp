from sklearn import datasets
from pprint import pprint # pretty print
import pandas as pd
import numpy as np

#------------------------------------------------------#
# All-the-news data set. CSV format
# https://www.kaggle.com/snapcrack/all-the-news
df1 = pd.read_csv("./data/all-the-news/articles1.csv") #. refers to the current folder
df2 = pd.read_csv("./data/all-the-news/articles2.csv") #. refers to the current folder
df3 = pd.read_csv("./data/all-the-news/articles3.csv") #. refers to the current folder
df1.shape
df2.shape
df3.shape
df1.head(5)
df2.head(5)
df3.head(5)

# Must reset index, otherwise it will not just stack everything up
df = pd.concat([df1, df2, df3], axis=0).reset_index(drop=True)
df.head(5)
df.shape
df.index

# Rename the columns
df = df.rename(columns = {"Unnamed: 0":"index"})
df.head(5)

# This news dataframe is good, ready to be used for model training.






















#------------------------------------------------------#
# Below is for practice only.
# Below codes are Not in use, as it is not really the news report data.
# The 20 newsgroups data set.
newsgroups_train = datasets.fetch_20newsgroups(subset="train")

pprint(list(newsgroups_train.target_names))

newsgroups_train.filenames.shape # each file name
newsgroups_train.target.shape # Corresponds to a type
newsgroups_train.target[:10]

# https://www.jianshu.com/p/244180c064cf
# https://www.jianshu.com/p/5bea552fbc46?utm_campaign=maleskine&utm_content=note&utm_medium=seo_notes&utm_source=recommendation
pprint(newsgroups_train.filenames)
pprint(newsgroups_train.target)
# No Idea how to access each post separately...
# Not really what we want


#------------------------------------------------------#
# Google has its pre-trained word2vec model based on GOOGLE NEWS
# http://mccormickml.com/2016/04/12/googles-pretrained-word2vec-model-in-python/
