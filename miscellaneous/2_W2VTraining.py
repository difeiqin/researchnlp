import spacy
import re
import numpy as np
import pandas as pd
import pickle
from gensim.models import Word2Vec
from pprint import pprint
# Must install en_core_wb_sm model first, in order to tokenize phrases correctly. see https://spacy.io/usage/models

nlp = spacy.load("en")
spacy_stopwords = spacy.lang.en.stop_words.STOP_WORDS # Get the stop words

def is_stopwords_or_synbols(input_u): # input must be unicode
    if input_u in spacy_stopwords:
        return True
    if re.match("[^a-zA-Z]\\.*", input_u): # check if it is symbol/space
        return True
    return False


# Preparing inputs...(Is it possible to use  data streaming to increase the speed?)
np.random.seed(0)
index_arr = list(range(df.shape[0]))
np.random.shuffle(index_arr)
index_to_use = index_arr[:int(len(index_arr)/10)]

input_list = [] # for storing all the sentences.
i = 0
content_to_use = df.content[index_to_use]
for cur_text in df.content[index_to_use]:
    u_list = nlp(cur_text)
    result_list = []
    for u_word in u_list:
        if not is_stopwords_or_synbols(u_word.text):
            result_list.append(u_word.text.lower())
    input_list.append(result_list)
    print("finish add in tokeniztaion result ..." + str(i))
    i += 1


# min_count: the min. number of occurance for w2c to include the word into vocab.
# size, the dimension. By default it's 100
w2v_model_news = Word2Vec(input_list, size=100, window=5, min_count=5, workers=4)

# Obtain the word vector by w2v_model_new.wv["input_word"]
# You may also check the similarity usiing w2v_model_news.wv.most_similar()

# w2v_model_news.save("./data/myModel-10-percent-2")


## Tokenize the article titles as well
titles_to_use = np.array(df.loc[index_to_use,"title"])
title_tokens_df = []
for i, cur_title in enumerate(titles_to_use):
    if pd.isnull(cur_title):
        title_tokens_df.append([])
        continue
    u_list = nlp(cur_title)
    result_list = []
    for cur_u in u_list:
        if not is_stopwords_or_synbols(cur_u.text):
            result_list.append(cur_u)
    title_tokens_df.append(result_list)
title_tokens_df = pd.DataFrame(title_tokens_df, index=None, columns=None)
title_tokens_df.to_csv("./data/tokenized_titles", sep=",")