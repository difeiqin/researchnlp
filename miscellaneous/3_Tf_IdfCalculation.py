# adjusted for doc length
import numpy as np
import pandas as pd
import pickle
from gensim.models import Word2Vec

w2v_model_news = Word2Vec.load("./data/myModel-10-percent")
original_index_arr = np.array(pd.read_csv("./data/index_to_use.csv", names=["index"]).loc[:, "index"]).astype(
    np.int).tolist()
with open('./data/tokenized_news_list', 'rb') as f:
    input_list = pickle.load(f)

vocab = w2v_model_news.wv.vocab
vocab_list = np.array(list(vocab))
content_to_use = np.array(df.content[index_to_use])


# index_to_use, the index array

# input_list, contains tokenization result of each piece of news.
def construct_dict_from_doc(input_token_list):
    temp = {}
    for this_token in input_token_list:
        if this_token in temp:
            temp[this_token] += 1
        else:
            temp[this_token] = 1
    return temp


terms_freq_matrix = np.zeros(shape=(len(input_list), len(vocab_list)))
# Index: (doc_index, vocab_index)

dict_list = []
for cur_token_list in input_list:
    dict_list.append(construct_dict_from_doc(cur_token_list))

## Compute the terms freq. matrix,
for i, cur_dict in enumerate(dict_list):
    for j, cur_word in enumerate(vocab_list):
        if cur_word in cur_dict:
            terms_freq_matrix[i][j] = cur_dict[cur_word]
        else:
            terms_freq_matrix[i][j] = 0
    print("End of dict ", i)

## Compute the terms freq. matrix adjusted by document length
doc_length_array = np.array([len(x) for x in input_list])[:, np.newaxis]
doc_length_matrix = np.repeat(doc_length_array, len(vocab_list), axis=1)
terms_freq_matrix_adj_by_length = terms_freq_matrix / doc_length_matrix

## Compute the inverse docs frequency matrix
docs_freq_arr = np.sum(terms_freq_matrix > 0, axis=0)
# inverse_docs_freq_arr = 1.0 / docs_freq_arr
inverse_docs_freq_arr = np.log(terms_freq_matrix.shape[0] / (docs_freq_arr + 1))
inverse_docs_freq_matrix = inverse_docs_freq_arr[np.newaxis, :]

TF_IDF_matrix = np.multiply(terms_freq_matrix, inverse_docs_freq_matrix)
TF_IDF_matrix_adj_by_length = np.multiply(terms_freq_matrix_adj_by_length, inverse_docs_freq_matrix)

# (array([  279,  4412,  4419, 11387, 12293, 12782, 13705, 14154]),)
# index where zero & nan. Ignore them for now.
# Get the top-10 key words...
top_index_matrix = np.argpartition(TF_IDF_matrix * -1, kth=10, axis=1)[:, :10]
top_index_matrix_adj_by_length = np.argpartition(TF_IDF_matrix_adj_by_length * -1, kth=10, axis=1)[:, :10]

top_weight_words = vocab_list[top_index_matrix]
top_weight_words_adj_by_length = vocab_list[top_index_matrix_adj_by_length]

## Print the top key words for article 0
top_weight_words[0]
top_weight_words_adj_by_length[0]

## Store the above results for future use
pd.DataFrame(top_weight_words).to_csv("./data/top_weight_words", header=None, index=None)
pd.DataFrame(top_weight_words_adj_by_length).to_csv("./data/top_weight_words_adj_by_length", header=None, index=None)

# equality_check_matrix = top_weight_words != top_weight_words_adj_by_length
# np.where(np.sum(equality_check_matrix, axis=1) > 0)
