import numpy as np
import pandas as pd
from gensim.models import Word2Vec

w2v_model_news = Word2Vec.load("./data/myModel-10-percent")
titles_to_use = np.array(df.loc[index_to_use, "title"])
tokenized_titles_df = pd.read_csv("./data/tokenized_titles", names=None, index_col=None).astype(np.str)
top_weight_words = pd.read_csv("./data/top_weight_words", header=None, index_col=None).astype(np.str)
top_weight_words_adj_by_length = pd.read_csv("./data/top_weight_words_adj_by_length", header=None,
                                             index_col=None).astype(np.str)

Mock_Notes = ["Japan", "Microsoft", "Treasury", "Equity"]


def find_average_similarity(input_word, row_series, w2c_model):
    score_list = find_similarity_vector(input_word, row_series, w2c_model)
    return np.mean(score_list)


def find_similarity_vector(input_word, row_series, w2c_model):
    score_list = []
    for word in row_series:
        score_list.append(w2c_model.wv.similarity(input_word, word))
    return score_list


def is_in_vocab(input_word, w2c_model):
    return input_word.lower() in w2v_model_news.wv.vocab


score_matrix = np.zeros(shape=(len(Mock_Notes), len(top_weight_words)))

for i in range(score_matrix.shape[0]):
    for j in range(score_matrix.shape[1]):
        if is_in_vocab(Mock_Notes[i], w2v_model_news):
            target_series = top_weight_words.loc[j]
            # target_series = pd.Series.append(top_weight_words.loc[j], tokenized_titles_df.loc[j])
            target_series = target_series[target_series != "nan"]
            score_matrix[i, j] = find_average_similarity(Mock_Notes[i].lower(), target_series, w2v_model_news)

score_matrix_max_index = np.argsort(-1 * score_matrix, axis=1)

for i in range(len(Mock_Notes)):
    print("Current word in Mock Notes :" + Mock_Notes[i])
    with open("./result/withoutTitle/" + Mock_Notes[i] + ".txt", "wb") as f:
        for j in range(10):
            f.write(str.encode(titles_to_use[score_matrix_max_index[i][j]]))
            f.write(str.encode("\n"))
            f.write(str.encode("Average similarity : " + str(score_matrix[i][score_matrix_max_index[i][j]])))
            f.write(str.encode("\n"))
            temp_df = pd.DataFrame([list(top_weight_words.loc[score_matrix_max_index[i][j]]),
                                    find_similarity_vector(Mock_Notes[i].lower(),
                                                           top_weight_words.loc[score_matrix_max_index[i][j]],
                                                           w2v_model_news)]).transpose()
            pd.DataFrame.sort_values(temp_df, by=[1], inplace=True, ascending=False)
            f.write(str.encode(str(temp_df)))
            f.write(str.encode("\n"))
            f.write(str.encode(content_to_use[score_matrix_max_index[i][j]]))
            f.write(str.encode("\n\n\n"))

score_matrix_with_title = np.zeros(shape=(len(Mock_Notes), len(top_weight_words)))
for i in range(score_matrix_with_title.shape[0]):
    for j in range(score_matrix_with_title.shape[1]):
        if is_in_vocab(Mock_Notes[i], w2v_model_news):
            # target_series = top_weight_words.loc[j]
            target_series = pd.Series.append(top_weight_words.loc[j], tokenized_titles_df.loc[j], ignore_index=True)
            target_series_processed = filter(lambda x: x != "nan" and x in w2v_model_news.wv.vocab, target_series)
            score_matrix_with_title[i, j] = find_average_similarity(Mock_Notes[i].lower(), target_series_processed,
                                                                    w2v_model_news)

score_matrix_max_index_with_title = np.argsort(-1 * score_matrix_with_title, axis=1)

for i in range(len(Mock_Notes)):
    print("Current word in Mock Notes :" + Mock_Notes[i])
    with open("./result/withTitle/" + Mock_Notes[i] + ".txt", "wb") as f:
        for j in range(10):
            f.write(str.encode(titles_to_use[score_matrix_max_index_with_title[i][j]]))
            f.write(str.encode("\n"))
            f.write(str.encode(
                "Average similarity : " + str(score_matrix_with_title[i][score_matrix_max_index_with_title[i][j]])))
            f.write(str.encode("\n"))
            target_series = pd.Series.append(top_weight_words.loc[score_matrix_max_index_with_title[i][j]],
                                             tokenized_titles_df.iloc[score_matrix_max_index_with_title[i][j],1:],
                                             ignore_index=True)
            target_series = target_series.map(lambda x: x.lower())
            target_series_processed = filter(lambda x: x != 'nan' and x in w2v_model_news.wv.vocab and x not in spacy_stopwords,target_series)
            target_series_processed = [x for x in target_series_processed]
            temp_df = pd.DataFrame([target_series_processed,find_similarity_vector(Mock_Notes[i].lower(),target_series_processed, w2v_model_news)]).transpose()
            pd.DataFrame.sort_values(temp_df, by=[1], inplace=True, ascending=False)
            f.write(str.encode(str(temp_df)))
            f.write(str.encode("\n"))
            f.write(str.encode(content_to_use[score_matrix_max_index_with_title[i][j]]))
            f.write(str.encode("\n\n\n"))
