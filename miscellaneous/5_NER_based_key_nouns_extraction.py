# The original plan was to construct a intent-classifier on meeting minutes,
# So we can categorize each line of meeting minutes into intents and search the intent as well as the keyword in all news.

# Problem is, we don't have enough data (sample meeting minutes) to build an intent-classifier....
# Or to do topic modeling (LDA)
# So...

# Switch to a different strategy. Using pre-trained NER model to extract named-entities (Take care of case-issue using truecaser)
# The STRONG assumption here is that, the important part of a meeting record are the "nouns & noun phrases",
# not verbs, nor adjectives or anything else.

# Use pre-trained NLP model to extract named-entites
# Also use the same model to extract all noun_chuncks.
# Prioritize noun_chuncks abased on whether it contains any named_entities or not (This is to handle the "Chinese housing market" case)
# Search each noun_chuncks in the news data set.
# Using mean W2C cosine similarity...

import spacy
import pandas as pd
import pickle
import numpy as np
import csv
from pprint import pprint

nlp = spacy.load('en_core_web_lg')  # Load the large model.
spacy_stopwords = spacy.lang.en.stop_words.STOP_WORDS  # Get the stop words

keyphrases = []
raw_minutes = []
with open("./data/sample_minutes.txt", "r") as f:
    raw_minutes = f.readlines()

def CombineEntitiesAndNounChunks(entities, noun_chunks):
    result_1 = []  # The highest priority result list
    for cur_noun_chunk in noun_chunks:
        for cur_entity in entities:
            if cur_entity.text in cur_noun_chunk.text:
                result_1.append(cur_noun_chunk)
                break;
    # The secondary list, filter out those that exactly match with what is in result_1
    result_2 = list(filter(lambda x: x.text not in list(map(lambda y: y.text, noun_chunks)), entities))
    # The third list, the remaining phrases.
    result_3 = [x for x in noun_chunks if x not in result_1]
    return [result_1, result_2, result_3]  # In descending order of importance


for cur_minutes in raw_minutes:
    processed = nlp(cur_minutes)
    entities = processed.ents
    noun_chunks = [x for x in processed.noun_chunks]
    processed_nouns = CombineEntitiesAndNounChunks(entities, noun_chunks)
    keyphrases.append(processed_nouns)


# Filter out the first stop word for noun phrases.
def FilterInitialStopWords(keyphrases):
    # Only look at 0-th and 2-th of the list, because they are phrases;
    for i, curLine in enumerate(keyphrases):
        phraseList1 = curLine[0]
        phraseList2 = curLine[2]
        filteredList1 = list(map(lambda x: x[1:] if x[0].text in spacy_stopwords else x, phraseList1)) \
            if len(phraseList1) > 0 else phraseList1
        filteredList2 = list(map(lambda x: x[1:] if x[0].text in spacy_stopwords else x, phraseList2)) \
            if len(phraseList2) > 0 else phraseList2
        keyphrases[i][0] = filteredList1
        keyphrases[i][2] = filteredList2


FilterInitialStopWords(keyphrases)
pprint(keyphrases)

keyphrasesDf = pd.DataFrame(keyphrases)
keyphrasesDf.to_csv("./result/sample_minutes_keywords_result.csv", decimal=",", header=False, index=False)


## Start keyword searching: Is it a searching problem or is it a NLP W2V problem?
# 6.1 Use W2V cosine similarity, calculate the mean similarity between minutes & article keywords? (Consider lemmatization)
#     Will have to train a new W2V model, because the previous tokenization does not do chunking properly.
# 6.2 Directly use elastic search? (Consider synonym plugin?)


# Convert each keyphrases token into string, (currently it is span/token
def convertToString(keyPhrases):
    strList = []
    for curMinuteLists in keyPhrases:
        curLists = []
        for curList in curMinuteLists:
            if len(curList) == 0:
                curLists.append([])
            else:
                tempList = list(map(lambda x: str(x), curList))
                curLists.append(tempList)
        strList.append(curLists)
    return strList


minutesKeywords = convertToString(keyphrases)

with open("./result/minutesKeywords", "wb") as f:
    pickle.dump(minutesKeywords, f)
# dump and load directly work with files; dumps&loads work with bytestr in memory

# with open("./result/minutesKeywords", "rb") as f:
#     testKeywords = pickle.load(f)
