# First, let's see weather the pre-trained model containes the phrase "New Yokr" "new york" or not...

import spacy
import pandas as pd
import numpy as np
import re
import csv
import pickle

from pandas.core import series
from pprint import pprint
from gensim.models import Word2Vec

nlp = spacy.load('en_core_web_lg')  # Load the large model.
spacy_stopwords = spacy.lang.en.stop_words.STOP_WORDS  # Get the stop words

# First, a little bit of experiment, to verify that the pre-trained model indeed does not contain "new york"-ish chunks
# Check does it contain New York
nlp.vocab.has_vector("new")
nlp.vocab.has_vector("york")
nlp.vocab.has_vector("York")
nlp.vocab.has_vector("New")
nlp.vocab.has_vector("New York")  # False, i.e. does not contain.

sample_sent = "I have been to so many different places in the world, yet New York is the one that really stands out."
# sample_sent = 'Apple is looking at buying U.K. startup for $1 billion'
processed_sent = nlp(sample_sent)
for x in processed_sent:
    print(x, x.ent_iob, x.ent_type)

# You can use label to tell which category of NER this chunk is
# Or you can directly look at each token and check its token.ent_type_ for this token's NER type (String)
# Check https://spacy.io/api/annotation#named-entities for complete list of NER types in spaCy
ents = [[ent.text, ent.start_char, ent.end_char, ent.label_] for ent in processed_sent.ents]
print(ents)


# For our own tokenizer, we would like it to group ORG, GPE phrases together as one token
chunkTypesKept = ["ORG", "GPE"]

def isNotStopwordOrSymbol(inputStr):
    if inputStr in spacy_stopwords:
        return False
    # match look at the start of the string; search look at anywhere in the string for a match.
    re_match = re.match(r"[a-zA-Z]+", inputStr)
    if re_match is not None and re_match.end() == len(inputStr):
        return True
    return False


def isInVocab(vocab, curWord):
    return curWord in vocab

def tokenizeAndKeepChuncks(inputSent):
    curDoc = nlp(inputSent)
    # Construct phrases

    tokensWithChunks = []
    i = 0
    while i < len(curDoc):
        curToken = curDoc[i]
        if curToken.ent_type_ not in chunkTypesKept:
            # Filter stopwords
            if isNotStopwordOrSymbol(curToken.text):
                tokensWithChunks.append(curToken.text)
            i = i + 1
            continue
        else:  # extract chunk from original sentence
            begin_idx = curToken.idx
            end_idx = curToken.idx + curToken.__len__()
            curEntType = curToken.ent_type_

            i = i + 1  # try to find the end of this chunk, starting from immediate next token
            while i < len(curDoc):
                # Look at its next token type
                curToken = curDoc[i]
                if curToken.ent_type_ == curEntType:
                    i = i + 1
                else:  # the current token (i.e. i-th), is the very end of this chunk
                    break;
            curToken = curDoc[i - 1]
            end_idx = curToken.idx + curToken.__len__();
            chunkStr = inputSent[begin_idx:end_idx]
            # Go through regex to remove beginning & ending symbols, if any
            matchedResults = re.match('[^a-zA-Z]*(.*\w)[^a-zA-Z]*', chunkStr)
            if matchedResults == None:
                continue
            else:
                filteredChunk = matchedResults.groups()[0]
                tokensWithChunks.append(filteredChunk)
    return tokensWithChunks


# Ready to train the model.
# Use 1_loadrawdata.py to first read in all the data. Then proceed with the following steps:
index_arr = np.array(pd.read_csv("./data/index_to_use.csv", names=["index"]).loc[:, "index"]).astype(
    np.int).tolist()
contentAndTitleSeries = (df['content'] + " " + df['title'])[index_arr]
contentAndTitleSeries.reset_index(drop=True, inplace=True)

tokenizedList = []
for i in range(len(contentAndTitleSeries)):
    if pd.isnull(contentAndTitleSeries[i]):
        tokenizedList.append([])
    else:
        tokenizedList.append(tokenizeAndKeepChuncks(contentAndTitleSeries[i]))
    print("Finish... " + str(i) + "-th")
with open("./data/tokenized_text_n_title_with_chunks_list.csv", "w", encoding="utf-8") as f:
    w = csv.writer(f, quoting=csv.QUOTE_ALL)
    for curR in tokenizedList:
        w.writerow(curR)


# Train the W2V model, use gensim
from gensim.models import Word2Vec
w2v_model_news_with_chunks = Word2Vec(tokenizedList, size=100, window=5, min_count=5, workers=4)
w2v_model_news_with_chunks.save("./data/myModel-10-percent-with-chunks")
