import spacy
import pandas as pd
import numpy as np
import csv
import pickle
from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer
from pandas.core import series
from pprint import pprint
from gensim.models import Word2Vec

with open("./data/tokenized_text_n_title_with_chunks_list.csv", "r", encoding="utf-8") as f:
    r = csv.reader(f)
    tokenizedList = list(r)
with open("./result/minutesKeywords", "rb") as f:
    minutesKeywords = pickle.load(f)
index_arr = np.array(pd.read_csv("./data/index_to_use.csv", names=["index"]).loc[:, "index"]).astype(np.int).tolist()

w2v_model_news_with_chunks = Word2Vec.load("./data/myModel-10-percent-with-chunks")

# difference between tfidfvectorizer & tfidftransformer?
# https://www.quora.com/What-is-the-difference-between-tfidfVectorizer-and-TfidfTransformer-And-what-about-their-output
tfidf = TfidfVectorizer(analyzer="word", tokenizer=lambda x: x, preprocessor=lambda x: x, token_pattern=None)
tfidf.fit(tokenizedList)
score_matrix = tfidf.transform(tokenizedList)  # type: 'scipy.sparse.csr.csr_matrix'

# Sort this matrix in each row, get argmax, extract keyword and corresponding score.
sorted_indices = []
for r in score_matrix:
    sorted_indices.append(sorted(r.indices, key=lambda i: r[0, i], reverse=True))
topKeywordMatrix = np.empty(shape=(0, 15))
map(lambda r1, r2: r2[0, r1].toarray()[0:15], sorted_indices, score_matrix)


def getKeywordsAndScore(sorted_indices, score_matrix, feature_names, topK):
    feature_names = np.array(feature_names)
    resultList = []
    for i, curR in enumerate(score_matrix):
        if curR.count_nonzero() >= topK:
            K = topK
        else:
            K = curR.count_nonzero()
        curSortedIndex = sorted_indices[i][:K]
        scoreArray = curR[0, curSortedIndex].toarray()[0]
        featuresArray = feature_names[curSortedIndex]
        curResult = list(map(lambda x, y: (x, y), featuresArray, scoreArray))
        resultList.append(curResult)
    return resultList


# Each row is a list of tuple(keyword, score), store the keyword list
results = getKeywordsAndScore(sorted_indices, score_matrix, tfidf.get_feature_names(), 15)

with open("./data/tokenized_news_list_with_chunks.csv", "w", encoding="utf-8") as f:
    w = csv.writer(f, quoting=csv.QUOTE_ALL)
    for curR in results:
        w.writerow(curR)

articleKeywords = results
# Compute the similarity between minutes keywords and each news article's keywords:
# Need to fix "white house" & "the white house" issue.
wv = w2v_model_news_with_chunks.wv


# keyphrases, is the list of meeting minutes keyphrases
def calculateMeanSimilarity(curKeyPhrase, keywordList, wv):
    # what if curTuple[0] not in wv.vocab
    scores = list(
        map(lambda curTuple: wv.similarity(curTuple[0], curKeyPhrase) if curTuple[0] in wv.vocab else 0, keywordList))
    scores = np.array(scores)
    return np.sum(scores) / np.sum(scores != 0)


### inpputList contains a list of keyphrases extracted from minutes (one of the three lists)
def matchArticleKeywords(keyPhrases, articleKeywords, wv):
    vocab = wv.vocab
    allScores = np.zeros(shape=(0, len(articleKeywords)))
    if len(keyPhrases) == 0:
        return np.empty(shape=(0, len(articleKeywords)))
    else:
        for i, curPhrase in enumerate(keyPhrases):  # each row in allScores
            curPhrase = str(curPhrase)
            if curPhrase not in vocab:
                allScores = np.append(allScores, np.zeros(shape=(1, len(articleKeywords))), axis=0)
                continue
            else:
                curPhraseScoreList = list(
                    map(lambda kyList: calculateMeanSimilarity(curPhrase, kyList, wv), articleKeywords))
                allScores = np.append(allScores, np.array(curPhraseScoreList)[np.newaxis, :], axis=0)
    return allScores


a = matchArticleKeywords(minutesKeywords[0][2], articleKeywords, wv)
meanSim = np.mean(a, axis=0)
sortedIndexArray = np.argsort(-1 * meanSim)


# Try to retrieve the keywords by sortedIndexArray
def printKeywordsAndTitle(inputArr):
    for i in inputArr:
        curRow = df.iloc[index_arr[i]]
        print("Title: " + curRow['title'])
        print("Mean similarity score: " + str(meanSim[i]))
        print("Keywords:")
        pprint(articleKeywords[i])


def saveKeywordsAndTitle(inputArr, f):
    for i in inputArr:
        curRow = df.iloc[index_arr[i]]
        f.write("Title: " + curRow['title'].__repr__() + "\n")
        f.write("Mean similarity score: " + str(meanSim[i]) + "\n")
        f.write("Keywords:" + "\n")
        pprint(articleKeywords[i], stream=f)
        f.write("\n\n")


with open("./result/meansSimilarities.txt", "w", encoding="utf-8") as f:
    f.write("Current input meeting minutes are... \n")
    f.write(raw_minutes[0].__repr__() + "\n\n\n")
    saveKeywordsAndTitle(sortedIndexArray[:10], f)

printKeywordsAndTitle(sortedIndexArray[:10])