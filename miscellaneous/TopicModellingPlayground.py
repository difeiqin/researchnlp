from nltk.corpus import brown
from gensim import models, corpora
from pprint import pprint
import re
import spacy

nlp = spacy.load("en_core_web_lg")
stop_words = spacy.lang.en.stop_words.STOP_WORDS
compiled_re = re.compile(r'[a-zA-Z\-][a-zA-Z\-]{2,}')  # regex for tokenization

data = []

for fileid in brown.fileids():
    document = ' '.join(brown.words(fileid))
    data.append(document.lower())

NO_DOCUMENTS = len(data)
NO_TOPICS = 10
tokenized_data = []


def clean_text(textDoc):
    textDoc = data[0]
    tokenized_text = nlp(textDoc)
    # How to do regex mapping in python?
    filtered_tokens_gen = (filter(lambda x: x.text not in stop_words and compiled_re.match(x.text), tokenized_text))
    # Map token to str. Note here how fitler and map are chained together... via generater/iterator
    cleaned_text = list(map(lambda x: x.text, filtered_tokens_gen))
    return cleaned_text


for textDoc in data:
    tokenized_data.append(clean_text(textDoc))

## Use gensim to build dictionary. then construct corpus
# Since we need to use gensim to build model, then we first neet to convert eveything into gensim stuffs (corpus, dictionary. etc)
# Build a dictionary based on result of tokenization. (here "new york" still separate as two words
# If you want new york to be one work, should do chunking before building the dict here.
dictionary = corpora.Dictionary(tokenized_data)
# A dictionary has its values stored in alpabetical order. Increasing.
corpus = [dictionary.doc2bow(text) for text in tokenized_data]
# Convert the text into list of word index, based on the dictionary index. It's a bag-of-words, thus order does not matter.
# Here the text must be a list of str

## Latent Dirichlet Allocation
# the corpus is a gensim object.
lda_model = models.LdaModel(corpus=corpus, num_topics=NO_TOPICS, id2word=dictionary)

## Latent Semantic Analysis
lsa_model = models.LsiModel(corpus=corpus, num_topics=NO_TOPICS, id2word=dictionary)

## Print topics
pprint(lda_model.print_topics())