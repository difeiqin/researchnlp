# Use iterator/iterables streaming to read file from disk and conduct training
# data streaming, aka lazy evaluation. IT SAVES MEMORY.
# Also, generators can be fed to anther generators, creating long, data-drive pipelines.

numbers = range(10000)
sum([n**2 for n in numbers])
# This is List Comprehension. What happens here is that,
# first it creates an array of n**2, then sum it.
# Wasteful, as it explicitly creates an array first.


# Generator: sqaure and sum one value after anther.
# No extra array created. Lazily evaluated stream of numbers
sum(n**2 for n in numbers)


# To build an iterator in Python, one has to implement a class with __iter__() and __next__(), keep
# track of internal states, raise StopIteration when no values to be returned. etc...
# What is python generator?
# It is a functiont hat returns an obj (itr) which we can use to iterate over a set of values.
# It provides an easy way to create iterator. All the overhead we mentioned above are taken care
# of automatically.

# It differs from a normal function in a sense that it does not return value;
# It "yield sth" and we can use next() to iter thorugh it.

# Generators can be created easily on the fly. Just like lambda exp creates anonymous function,
# generator expression creates anonymous generator function on the fly.

# The major difference between a list comprehension and a generator expression is that
# while list comprehension produces the entire list, generator expression produces one item at a time.
# Which makes it much more memory-efficient than its equivalent list comprehension.


# Generator does not return what you want immediately, instead it returns result
# one by one on demand.
my_list = [1,2,3,4]
a = (x**2 for x in my_list) #note how to USE ()

for cur in a: # In python we often use for loop, not while, with generator.
    print (cur)

# NOTE that generator can only be used once. i.e. after finish looping, it is finished.
# If you want to use the same generator again, you have to initialize it again.
# HOWEVER, if it is an iterator, it creates a new iterator every time it is looped over.
temp = next(a)
while (temp):
    print(temp)
    temp = next(a)

# https://www.programiz.com/python-programming/generator
# It can be used to generate infinite stream, since list cannot support it.
# Thus it is necessary when you don't know how much data you have.
# Or when you may have new data come in (https://rare-technologies.com/data-streaming-in-python-generators-iterators-iterables/)

# For pipelines:
with open('sells.log') as file:
    pizza_col = (line[3] for line in file) #1st
    per_hour = (int(x) for x in pizza_col if x != 'N/A') #2nd
    print("Total pizzas sold = ",sum(per_hour))


# USE generator feed data into gensim, then you will not have to load entire data file into memory.





# The normal method of reading data...
# 1> read everything into memory, combined in one DF.
from pprint import pprint
import os
df1 = pd.read_csv("./data/all-the-news/articles1.csv") #. refers to the current folder
df2 = pd.read_csv("./data/all-the-news/articles2.csv") #. refers to the current folder
df3 = pd.read_csv("./data/all-the-news/articles3.csv") #. refers to the current folder

pprint([df1.shape, df2.shape, df3.shape])
df1.head(5)
df2.head(5)
df3.head(5)

df = pd.concat([df1, df2, df3], axis=0).reset_index(drop=True)
df.head(5)
df.shape
df.index


# 2> Use generator to read file one by one, and one line at a time
with open("./data/all-the-news/articles1.csv", encoding="utf-8") as f:
    i = 0
    for line in f:
        print(type(f))
        print(line)
        i += 1
        if i >= 10:
            break;

# for cur_file in os.listdir("./"): # a list of all the filenames under this folder.

import re
from gensim.models import Word2Vec

model = Word2Vec([["a"]], size=100, window=5, min_count=1, workers=4) #need a proper initialization.

for root, dirs, files in os.walk("./data"): # automatically list all the (dirs & files) under one root folder.
    # print(root, dirs, files)
    for fname in filter(lambda inputName: inputName.endswith(".csv"), files):
        with open(os.path.join(root, fname), encoding="utf-8") as f: # f is an itr
            next(f); #skip first line
            # print(f.readline())
            for l in f: # current line
                # Now you can feed in the line for training
                # Need to strip the stop word, finish all pre-processing.
                gettokens(model, l)

re0 = re.compile('"(.*?)"')
def gettokens(model, inputLine):
    article = re0.match(inputLine).group(1)
    # POS tag first, tokenization & remove stop words