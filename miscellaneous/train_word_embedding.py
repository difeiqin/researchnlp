# https://nlpforhackers.io/word-embeddings/
import nltk
from pprint import pprint
from nltk.corpus import brown
from gensim.models import Word2Vec

pprint(brown.words())
pprint(brown.sents())

# By default, 'sg=0', i.e. it is using CBOW; if you set sg = 1, then here will use skip gram
w2v_model = Word2Vec(brown.sents(), size=128, window=5, min_count=3, workers=4)

print(w2v_model.wv['Italy'])
print(w2v_model.wv['France'])

pprint(w2v_model.wv.most_similar("Paris"))

# Calculation, the result is not very good
# because the corpus is not big
pprint(w2v_model.wv.most_similar(positive=["king", "woman"], negative=["man"]))
pprint(w2v_model.wv.most_similar(positive=["Rome", "France"], negative=["Italy"]))

# Lets try a bigger corpus
from gensim.models.word2vec import Text8Corpus
# Go here and download + unzip the Text8 Corpus: http://mattmahoney.net/dc/text8.zip
# We take only words that appear more than 150 times for doing a visualization later
w2v_model_2 = Word2Vec(Text8Corpus('./data/text8'), size=100, window=5, min_count=150, workers=4)

# Test its performance
pprint(w2v_model_2.wv.most_similar("paris"))
pprint(w2v_model.wv.most_similar(positive=["king", "woman"], negative=["man"]))
pprint(w2v_model.wv.most_similar(positive=["girl", "father"], negative=["boy"]))
pprint(w2v_model.wv.most_similar(positive=["paris", "italy"], negative=["france"]))


# ------------------------------------------------------- #
# Visualize W2V with t-SNE from sklearn (t-distributed stochastic neighbour embedding) is used to
# diplay high-dimensional data in the 2D/3D.
# We will also use bokeh, interactive plotting package.
# There seems to be problems with bokeh under python 3.6...

import numpy as np
import pandas as pd
from sklearn.manifold import TSNE
from bokeh.io import push_notebook, show, output_notebook
from bokeh.plotting import figure
from bokeh.models import ColumnDataSource, LabelSet
output_notebook()

X = []
for word in w2v_model_2.wv.vocab:
    X.append(w2v_model_2.wv[word])

X = np.array(X)
print("Computed X: ", X.shape)
X_embedded = TSNE(n_components=2, n_iter=250, verbose=2).fit_transform(X)
pprint("Computed t-SNE :" + str(X_embedded.shape))

df = pd.DataFrame(columns=['x', 'y', 'word'])
df['x'], df['y'], df['word'] = X_embedded[:,0], X_embedded[:,1], w2v_model_2.wv.vocab

source = ColumnDataSource(ColumnDataSource.from_df(df))
labels = LabelSet(x="x", y="y", text="word", y_offset=8, text_font_size="8pt", text_color="#555555", source=source, text_align='center')

plot = figure(plot_width=600, plot_height=600)
plot.circle("x", "y", size=12, source=source, line_color="black", fill_alpha=0.8)
plot.add_layout(labels)
show(plot, notebook_handle=True)

# ------------------------------------------------------- #
# GLoVe (Global Vectors) is another method, with no ready-to-use implementation in most libraries
# We julcan make use of the pre-trained models, converges faster than Word2Vec

# ------------------------------------------------------- #
# FastText (developed by Facebook), serves two main purposes:
# 1. Learning of WV
# 2. Text Classification
# Also, fasttext allows guessing the unseen words (i.e. words not in corpus yet)

from gensim.models import FastText

ft_model = FastText(Text8Corpus("./data/text8"), size=100, window=5, min_count=150, workers=4, min_n=3, max_n=10)
pprint(ft_model.wv.most_similar("paris"))
pprint(ft_model.wv.most_similar(positive=['woman','king'], negative=['man']))
pprint(ft_model.wv.most_similar(positive=['father','female'], negative=['male']))

# Guess unseen word
print(ft_model.most_similar('veniciaaaaaa', topn=3))
